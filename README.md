## full_oppo6771_18611-user 9 PPR1.180610.011 eng.root.20200915.143154 release-keys
- Manufacturer: oppo
- Platform: mt6771
- Codename: RMX1831
- Brand: realme
- Flavor: fuse_RMX1831-eng
- Release Version: 12
- Id: SD1A.210817.036.A8
- Incremental: eng.ubuntu.20211209.132549
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/fuse_RMX1831/RMX1831:12/SD1A.210817.036.A8/ubuntu12091325:eng/test-keys
- OTA version: 
- Branch: full_oppo6771_18611-user-9-PPR1.180610.011-eng.root.20200915.143154-release-keys
- Repo: realme_rmx1831_dump_29576


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
